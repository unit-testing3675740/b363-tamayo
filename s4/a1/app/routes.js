const { names, users } = require('../src/util.js')

module.exports = (app) => {
  app.get('/', (req, res) => {
      return res.send({'data': {} });
  });

  app.get('/people', (req, res) => {
      return res.send({
          people: names
      });
  })

  app.post('/person', (req, res) => {
      if(!req.body.hasOwnProperty('name')){
          return res.status(400).send({
              'error': 'Bad Request - missing required parameter NAME'
          })
      }
      if(typeof req.body.name !== 'string'){
          return res.status(400).send({
              'error': 'Bad Request - NAME has to be a string'
          })
      }
      if(!req.body.hasOwnProperty('alias')){
        return res.status(400).send({
            'error': 'Bad Request - missing required parameter ALIAS'
        })
      }
      if(typeof req.body.name !== 'string'){
        return res.status(400).send({
            'error': 'Bad Request - ALIAS has to be a string'
        })
      }
      if(!req.body.hasOwnProperty('age')){
          return res.status(400).send({
              'error': 'Bad Request - missing required parameter AGE'
          })
      }
      if(typeof req.body.age !== "number"){
          return res.status(400).send({
              'error': 'Bad Request - AGE has to be a number'
          })	
      }
  })

  // S4 Activity Start
  app.post('/login', (req,res)=>{

    let foundUser = users.find((user) => {

        return user.username === req.body.username && user.password === req.body.password

    });

    if(!req.body.hasOwnProperty('username')){
        return res.status(400).send({
            'error': 'Bad Request - missing required parameter USERNAME'
        })
    }

    if(!req.body.hasOwnProperty('password')){
        return res.status(400).send({
            'error': 'Bad Request - missing required parameter PASSWORD'
        })
    }

    if(foundUser){
        return res.send({
            'message': 'Welcome back!' 
        })
    }

    if(!foundUser){
        return res.status(403).send({
            'error': 'Bad Request - wrong credentials'
        })
    }
  })
// S4 Activity End
}
