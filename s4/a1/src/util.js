function factorial(n) {
  if(n<0) return undefined;
  if(n===0) return 1;
  if(n===1) return 1;
  if(typeof n !== "number") return undefined;
  return n * factorial(n-1);
}

function div_check(n) {
  if(n%5===0) return true;
  if(n%7===0) return true;
  return false;
}

const names = {
  "Brandon": {
    "name": "Brandon Boyd",
    "alias": "Incubus",
    "age": 35
  },
  "Steve": {
    "name": "Steve Tyler",
    "alias": "Aerosmith",
    "age": 56
  }
}

// S4 Activity Start
const users = [
  {
      username: "brBoyd87",
      password: "87brandon19"

  },
  {
      username: "tylerofsteve",
      password: "stevenstyle75"
  }
]
// S4 Activity End

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names,
  users: users
}