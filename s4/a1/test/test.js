const { factorial, div_check } = require('../src/util.js');

// Gets the expect and assert function from chai to be used
const { expect, assert } = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {
  // "it" accepts two parameters
  // string explaining what the test should do
  // callback function which contains the actual test
  it('test_fun_factorial_5!_is_120', () => {
    const product = factorial(5);
    // "expect" - returning expected and actual value
    expect(product).to.equal(120);
  })

  it('test_fun_factorial_1!_is_1', () => {
    const product = factorial(1);
    // "assert" - checking if function is return correct results
    assert.equal(product, 1);
  })

  it('test_fun_factorial_0!_is_1', () => {
    const product = factorial(0);
    expect(product).to.equal(1);
  })

  it('test_fun_factorial_4!_is_24', () => {
    const product = factorial(4);
    assert.equal(product, 24);
  })

  it('test_fun_factorial_10!_is_3628800', () => {
    const product = factorial(10);
    expect(product).to.equal(3628800);
  })

  // Test for negative numbers
  it('test_fun_factorial_neg_1_is_undefined', () => {
    const product = factorial(-1);
    expect(product).to.equal(undefined);
  })

  it('test_fun_factorial_invalid_number_is_undefined', () => {
    const product = factorial('abc');
    expect(product).to.be.equal(undefined);
  })
})

describe('test_divisibility_by_5_or_7', () => {
  it('test_100_is_divisible_by_5', () => {
    const dividend = div_check(100);
    expect(dividend).to.equal(true);
  })

  it('test_49_is_divisible_by_7', () => {
    const dividend = div_check(49);
    assert.equal(dividend, true);
  })

  it('test_30_is_divisible_by_5', () => {
    const dividend = div_check(30);
    expect(dividend).to.equal(true);
  })

  it('test_56_is_divisible_by_7', () => {
    const dividend = div_check(56);
    assert.equal(dividend, true);
  })
})