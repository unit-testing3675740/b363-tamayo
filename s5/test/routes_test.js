const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('1_test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('2_test_api_post_currency_returns_400_if_no_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('3_test_api_post_currency_returns_400_if_name_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: 123,
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('4_test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "",
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('5_test_api_post_currency_returns_400_if_no_ex', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: "kwd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('6_test_api_post_currency_returns_400_ex_is_not_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: "kwd",
			ex: "peso: 184.38, won: 4371.85, yen: 485.28, yuan: 23.4"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('7_test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: "kwd",
			ex: {}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('8_test_api_post_currency_returns_400_if_no_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('9_test_api_post_currency_returns_400_if_alias_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: 51110,
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('10_test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "",
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('11_test_api_post_currency_returns_400_if_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Japanese Yen",
			alias: "yen",
			ex: {
				peso: 0.47,
				usd: 0.0092,
				won: 10.93,
				yuan: 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('12_test_api_post_currency_returns_200_if_no_dupes', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Kuwaiti Dinar",
			alias: "kwd",
			ex: {
				peso: 184.38,
				won: 4371.85,
				yen: 485.28,
				yuan: 23.4
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
})
