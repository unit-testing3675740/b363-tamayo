const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		let foundRate = Object.keys(exchangeRates).find(rate => {
			return rate === req.body.alias
		})

		if(!req.body.hasOwnProperty('name')) {
			return res.status(400).send({
					'error': 'Bad Request - missing required parameter NAME'
			})
		}

		if(typeof req.body.name !== 'string') {
			return res.status(400).send({
					'error': 'Bad Request - NAME has to be a string'
			})
		}

		if(req.body.name === '') {
			return res.status(400).send({
				'error': 'Bad Request - NAME should not be empty'
			})
		}

		if(!req.body.hasOwnProperty('ex')) {
			return res.status(400).send({
					'error': 'Bad Request - missing required parameter EX'
			})
		}

		if(typeof req.body.ex !== 'object') {
			return res.status(400).send({
					'error': 'Bad Request - EX has to be an object'
			})
		}

		if(Object.keys(req.body.ex).length === 0) {
			return res.status(400).send({
					'error': 'Bad Request - EX should not be empty'
			})
		}

		if(!req.body.hasOwnProperty('alias')) {
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter ALIAS'
			})
		}

		if(typeof req.body.alias !== 'string') {
			return res.status(400).send({
					'error': 'Bad Request - ALIAS has to be a string'
			})
		}

		if(foundRate) {
			return res.status(400).send({
				'error': 'Bad Request - already exists'
			})
		}

		if(!foundRate) {
			return res.status(200).send({
				'message': 'Successful'
			})
		}
	})
}
